﻿#define MANUAL_GPS

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class GPS : MonoBehaviour
{
    // @LeoDebug. LeoHomeGPS: 24.99688, 121.5427

    public DebugMoveGpsPanelCtrl _debugMoveGpsPanelCtrl;

    public Text latitude_t;
    public Text longitude_t;

    public float latitude;
    public float longitude;

    public Main main_sc;
    public bool opgps;
    public bool manualGPS = false;

    private float _lastUpdateTime = 0f;
    private bool _prevDebugMoveGpsPanelOpened = false;
    private bool _running = false;

    // Start is called before the first frame update
    void Start()
    {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
        {
            Permission.RequestUserPermission(Permission.CoarseLocation);
        }
        _lastUpdateTime = Time.realtimeSinceStartup;
#endif
        StartCoroutine(once_fn());

// #if MANUAL_GPS
//         opgps = false;
//         latitude = 24.994020f;
//         longitude = 121.538936f;
// #endif
    }

    // Update is called once per frame
    void Update()
    {
        if (_debugMoveGpsPanelCtrl != null)
        {
            if (_debugMoveGpsPanelCtrl.IsDirectionShowing())
            {
                main_sc.GetComponent<Main>().player_pos.GetComponent<RectTransform>().anchoredPosition = _debugMoveGpsPanelCtrl.GetPlayerPos();
                return;
            }
        }

        if (_running) {
            bool logLocation = false;
    #if PLATFORM_ANDROID
            if (Time.realtimeSinceStartup >= _lastUpdateTime + 2f)
            {
                if (!Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
                {
                    Debug.LogWarning("Has No premission of CoarseLocation.");
                }
                _lastUpdateTime = Time.realtimeSinceStartup;
                logLocation = true;

                //Debug.Log("location.isEnabledByUser=" + (Input.location.isEnabledByUser == true).ToString());
                //Debug.Log("location.status == LocationServiceStatus.Running=" + (Input.location.status == LocationServiceStatus.Running).ToString());

                Debug.LogFormat("<color=yellow>Check GPS !</color>");
                if (Input.location.isEnabledByUser == true && Input.location.status == LocationServiceStatus.Running)
                {
                    Debug.LogFormat("location la={0} lo={1}", Input.location.lastData.latitude, Input.location.lastData.longitude);
                }
            }
    #endif

            latitude_t.text = latitude.ToString();
            longitude_t.text = longitude.ToString();

            bool locationEnabled = false;
    #if PLATFORM_ANDROID
            locationEnabled = Permission.HasUserAuthorizedPermission(Permission.CoarseLocation);
    #endif

            if (Input.location.isEnabledByUser)
            {
                if (Input.location.status == LocationServiceStatus.Running)
                {
                    latitude = Input.location.lastData.latitude;
                    longitude = Input.location.lastData.longitude;

                    if (logLocation)
                    {
                        Debug.LogFormat("2 location la={0} lo={1}", Input.location.lastData.latitude, Input.location.lastData.longitude);
                    }
                }
            }

    #if DEBUG_LOG
            const float GPS_SHIFT = 0.005f;
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                longitude += GPS_SHIFT;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                longitude -= GPS_SHIFT;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                latitude += GPS_SHIFT;
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                latitude -= GPS_SHIFT;
            }
    #endif
        }
    }

    public void SetRunning(bool running) {
        _running = running;
    }

    IEnumerator once_fn()
    {
        while (true) {
            yield return new WaitForSeconds(1);
            if (_debugMoveGpsPanelCtrl != null)
            {
                if (_debugMoveGpsPanelCtrl.IsDirectionShowing())
                {
                    continue;
                }
            }
            
            for (int x = 0; x <= main_sc.pointlist.Length - 1; x++)
            {
                if (latitude <= main_sc.pointlist[x].GetComponent<point_SC>().latitude && longitude <= main_sc.pointlist[x].GetComponent<point_SC>().longitude)
                {
                    //playerpos.transform.localPosition = this.transform.localPosition;
                    main_sc.GetComponent<Main>().player_pos.transform.localPosition = main_sc.pointlist[x].transform.localPosition;
                    Debug.Log(main_sc.pointlist[x].transform.localPosition);
                    break;
                }
            }
        }
    }

    public void opengps_fn()
    {
        opgps = true;
        Debug.LogFormat("<color=yellow>Ask GPS Start!</color>");
        Input.location.Start();
    }

    public void closeapp_fn()
    {
        Input.location.Stop();
        Application.Quit();
    }
}
