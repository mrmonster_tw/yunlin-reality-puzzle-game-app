﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionPanelCtrl : MonoBehaviour
{
    [SerializeField] Text _textQuestion;
    [SerializeField] Text[] _textSelections;
    [SerializeField] GameObject _uiCorrectAnswer;
    [SerializeField] GameObject _uiWrongAnswer;
    [SerializeField] GameObject _contents;
    [SerializeField] Main _main;
    [SerializeField] GameObject _answers;

    public static QuestionPanelCtrl _instance;

    private string _questionName = string.Empty;
    private int _answerIndex = -1;
    private int _selectedIndex = -1;

    private void Start()
    {
        Show(false);
        _answers.SetActive(false);
    }

    public void Setup(string questionName, string question, string[] selections, int answerIndex)
    {
        if (_textQuestion != null) {
            _textQuestion.text = question;
            if (_textSelections != null && selections != null)
            {
                if (_textSelections.Length == selections.Length)
                {
                    for (int i = 0; i < _textSelections.Length; ++i)
                    {
                        _textSelections[i].text = selections[i];
                    }
                }
            }
        }

        _questionName = questionName;
        _answerIndex = answerIndex;
    }

    public void Show(bool show)
    {
        _contents.SetActive(show);
        if (!show)
        {
            Reset();
        }
    }

    private void Reset()
    {
        _answerIndex = -1;
        _selectedIndex = -1;
    }

    public void OnConfirmClicked()
    {
        if (_selectedIndex >= 0)
        {
            string questionPrefix = ParseQuestionName(_questionName);
            if (!PlayerDataCtrl._instance.HasOpenedQuestionName(questionPrefix))
            {
                Debug.Log("Not has questionPrefix=" + questionPrefix);
                PlayerDataCtrl._instance.AddOpenedQuestionName(questionPrefix);
                AppWebService._instance.RequestOpenedQuestionNew(questionPrefix, null, null);
            }

            if (_answerIndex == _selectedIndex)
            {
                _uiCorrectAnswer.SetActive(true);
                Show(false);
                //right.SetActive(true);
                //mark.SetActive(true);
                //Axe_q.SetActive(false);
                //Axe.GetComponent<BoxCollider2D>().enabled = false;
                //self.SetActive(false);

                PlayerDataCtrl._instance.AddScoreName(_questionName);
            }
            else
            {
                _uiWrongAnswer.SetActive(true);
            }
        }
    }

    public void OnSelectionClicked(int index)
    {
        _selectedIndex = index;
    }

    public void OnMarkClicked(GameObject mark)
    {
        if (mark != null)
        {
            string[] parameters = mark.name.Split('_');
            string markName = parameters[0];
            int questionNum = _main.q_ramdon;
            QuestionSettingCtrl.Question questionData = QuestionSettingCtrl._instance.GetQuestion(markName, questionNum);
            if (questionData != null)
            {
                int answerIndex = Random.Range(0, 4);
                List<string> selections = new List<string>();
                for (int i = 1; i < 4; i++)
                {
                    selections.Add(questionData.selections[i]);
                }
                selections.Insert(answerIndex, questionData.selections[0]);

                Setup(string.Format("{0}_{1}", markName, questionNum), questionData.question, selections.ToArray(), answerIndex);
                Show(true);
            }
        }
    }

    public void OnHelpClicked() {
        _answers.SetActive(true);
    }

    public void OnHelpBackClicked() {
        _answers.SetActive(false);
    }

    public static string ParseQuestionName(string name)
    {
        string[] parameters = name.Split('_');
        return parameters[0];
    }
}
