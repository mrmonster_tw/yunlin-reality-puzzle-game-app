﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class InitSceneManager : MonoBehaviour
{
	private const float SWITCH_SCENE_DELAY = 3f;

	private float _lastUpdateTime = 0f;
    private List<bool> _readyFlags = new List<bool>();
    private bool _readyToSwitchScene = false;

    void Start()
    {
        _lastUpdateTime = Time.realtimeSinceStartup;

        StartCoroutine(CheckInternetStatus((success) => {
            if (success) {
                _readyToSwitchScene = true;
            }
            else {
                PopupDialogCtrl._instance.Show("請確認網路連線狀態正常\n並重新啟動遊戲", 0f, null);
            }
        }));
    }

    void Update()
    {
        if (_lastUpdateTime != 0f && _readyToSwitchScene)
        {
            if (Time.realtimeSinceStartup >= _lastUpdateTime + SWITCH_SCENE_DELAY)
            {
                _lastUpdateTime = 0f;
                UnityEngine.SceneManagement.SceneManager.LoadScene("Yunlin");
            }
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.A))
        {
            //string url = "https://cdc620f9.ngrok.io/" + "yunlin_ar_puzzle_game/user/register";
            string url = "https://www.mrmonsters.com/yunlin_ar_puzzle_game.php";
            WWWForm wwwForm = new WWWForm();
            wwwForm.AddField("account", "leo");
            wwwForm.AddField("password", "leo111111");
            wwwForm.AddField("data", "fromUnity");

            HttpService.SendPost(url, wwwForm, string.Empty,
                (text) =>
                {
                    Debug.Log("register succeed.");
                },
                (text) =>
                {
                    Debug.Log("register failed!");
                }
            );
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            //string url = "https://cdc620f9.ngrok.io/" + "yunlin_ar_puzzle_game/user/register";
            string url = "https://www.mrmonsters.com/yunlin_ar_puzzle_game.php";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("page", "unity");

            HttpService.SendGet(url, parameters,
                (text) =>
                {
                    Debug.Log("register succeed.");
                },
                (text) =>
                {
                    Debug.Log("register failed!");
                }
            );
        }
#endif
    }

    private IEnumerator CheckInternetStatus(System.Action<bool> resultCallback) {
        yield return new WaitForEndOfFrame();

        var webRequest = UnityWebRequest.Get("https://www.google.com");
        yield return webRequest.SendWebRequest();

        if (!(webRequest.isNetworkError || webRequest.isHttpError)) {
            if (resultCallback != null) {
                resultCallback(true);
            }
        }
        else {
            if (resultCallback != null) {
                resultCallback(false);
            }
        }
    }

    // private void RequestTextCfg() {
    //     _readyFlags.Add(false);
    //     int flagIndex = _readyFlags.Count - 1;

    //     StartCoroutine(AppWebService._instance.RequestDownloadTxt());
    // }
}
