﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class FuncTestingManager : MonoBehaviour
{
    [System.Serializable]
    public class ResponseBase
    {
        public string message;
        public string data;
    }

    [SerializeField] private DatepickerCtrl _datepickerCtrl;

    private string _token = string.Empty;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMapClicked()
    {
        LaunchApp("com.google.Maps");
    }

    public void OnWebClicked()
    {
        Application.OpenURL("https://www.google.com/maps");
    }

    public void OnDateClicked()
    {
        if (_datepickerCtrl != null)
        {
            Debug.LogFormat("Date={0}-{1}-{2}", _datepickerCtrl.GetYear(), _datepickerCtrl.GetMonth(), _datepickerCtrl.GetDay());
        }
    }

    public void LaunchApp(string appBundleId)
    {
        // @Leo. NOT working, need fix!!!

        bool fail = false;
        string bundleId = appBundleId;
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

        if (up == null)
        {
            Debug.LogWarning("up == null");
        }

        if (ca == null)
        {
            Debug.LogWarning("ca == null");
        }

        if (packageManager == null)
        {
            Debug.LogWarning("packageManager == null");
        }

        AndroidJavaObject launchIntent = null;
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
        }
        catch (System.Exception e)
        {
            fail = true;
        }

        if (fail)
        { //open app in store
            Application.OpenURL("https://google.com");
        }
        else
        { //open the app
            ca.Call("startActivity", launchIntent);
        }

        up.Dispose();
        ca.Dispose();
        packageManager.Dispose();
        launchIntent.Dispose();
    }

    public void OnRequestNewClicked()
    {
        AppWebService._instance.RequestPlayerNew("billwang", "pw222222", "Bill", "男", "1989-11-22", "新北市",
            (text) =>
            {
                Debug.LogFormat("succeed. text={0}", text);
            },
            (text) =>
            {
                Debug.LogWarningFormat("failed. text={0}", text);
            }
        );
    }

    public void OnRequestLoginClicked()
    {
        //string url = "https://www.mrmonsters.com/yunlin_ar_puzzle_game.php";
        //WWWForm wwwForm = new WWWForm();
        //wwwForm.AddField("action", "playerLogin");
        //wwwForm.AddField("username", "leo3");
        //wwwForm.AddField("password", "leo333333");

        //HttpService.SendPost(url, wwwForm, string.Empty,
        //    (text) =>
        //    {
        //        Debug.LogFormat("succeed. text={0}", text);
        //        ResponseLogin resp = JsonUtility.FromJson<ResponseLogin>(text);
        //        _token = resp.token;
        //    },
        //    (text) =>
        //    {
        //        Debug.LogWarningFormat("failed. text={0}", text);
        //    }
        //);

        AppWebService._instance.RequestPlayerLogin("billwang", "pw222222",
            (text) =>
            {
                Debug.LogFormat("succeed. text={0}", text);
                AppWebService.ResponseLogin resp = JsonUtility.FromJson<AppWebService.ResponseLogin>(text);
                _token = resp.token;
            },
            (text) =>
            {
                Debug.LogWarningFormat("failed. text={0}", text);
            }
        );
    }

    public void OnLedONClicked() {
        HttpService.SendGet("http://192.168.43.167/led/on", null, null, null);
    }

    public void OnLedOFFClicked() {
        HttpService.SendGet("http://192.168.43.167/led/off", null, null, null);
    }

    public void OnRequestScoreNew()
    {
        //string url = "https://www.mrmonsters.com/yunlin_ar_puzzle_game.php";
        //WWWForm wwwForm = new WWWForm();
        //wwwForm.AddField("action", "playerScoreNew");
        //wwwForm.AddField("score_name", "景點AA");

        //HttpService.SendPost(url, wwwForm, _token,
        //    (text) =>
        //    {
        //        Debug.LogFormat("succeed. text={0}", text);
        //    },
        //    (text) =>
        //    {
        //        Debug.LogWarningFormat("failed. text={0}", text);
        //    }
        //);

        AppWebService._instance.RequestPlayerScoreNew("池塘",
            (text) =>
            {
                Debug.LogFormat("succeed. text={0}", text);
            },
            (text) =>
            {
                Debug.LogWarningFormat("failed. text={0}", text);
            }
        );
    }

    public void OnRequestScoreAll()
    {
        //string url = "https://www.mrmonsters.com/yunlin_ar_puzzle_game.php";
        //WWWForm wwwForm = new WWWForm();
        //wwwForm.AddField("action", "playerScoreAll");

        //HttpService.SendPost(url, wwwForm, _token,
        //    (text) =>
        //    {
        //        Debug.LogFormat("succeed. text={0}", text);
        //        if (!string.IsNullOrEmpty(text))
        //        {
        //            ResponsePlayerScores respScores = JsonUtility.FromJson<ResponsePlayerScores>(text);
        //            foreach (var name in respScores.names)
        //            {
        //                Debug.LogFormat("    name={0}", name);
        //            }
        //        }
        //    },
        //    (text) =>
        //    {
        //        Debug.LogWarningFormat("failed. text={0}", text);
        //    }
        //);

        AppWebService._instance.RequestPlayerScoreAll(
            (text) =>
            {
                Debug.LogFormat("succeed. text={0}", text);
                if (!string.IsNullOrEmpty(text))
                {
                    AppWebService.ResponsePlayerScores respScores = JsonUtility.FromJson<AppWebService.ResponsePlayerScores>(text);
                    foreach (var name in respScores.names)
                    {
                        Debug.LogFormat("    name={0}", name);
                    }
                }
            },
            (text) =>
            {
                Debug.LogWarningFormat("failed. text={0}", text);
            }
        );
    }

    string authenticate(string username, string password)
    {
        string auth = username + ":" + password;
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;
        return auth;
    }

    IEnumerator makeAuthRequest()
    {
        string authorization = authenticate("leo3", "leo333333");
        string url = "https://www.mrmonsters.com/yunlin_ar_puzzle_game.php";
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("action", "auth");

        UnityWebRequest www = UnityWebRequest.Post(url, wwwForm);
        www.SetRequestHeader("AUTHORIZATION", authorization);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            Debug.Log("Post complete! text=" + www.downloadHandler.text);
        }
    }
}
