﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickerCtrl : MonoBehaviour
{
    public enum DataType
    {
        Year,
        Month,
        Day
    }

    public class PickerItem
    {
        public string _displayText;
        public int _value;
    }

    [SerializeField] private Image _datepicker;
    [SerializeField] private RectTransform _pickerContainer;
    [SerializeField] private Text[] _uis;
    [SerializeField] private DataType _dateType;
    [SerializeField] private float _uiItemHeight = 25f;

    private List<PickerItem> _items;
    private int _currItemIndex = 0;
    private int _numItems = 0;

    //private int _currUiIndex = 0;
    private int _numUIs = 0;
    private bool _lerping = false;
    private float _targetY = 0f;
    private float _lastUpdateTime = 0f;
    private System.Action<int> _onValueChangedCallback = null;

    private void Start()
    {
        switch (_dateType)
        {
            case DataType.Year:
                {
                    int startYear = 1921;
                    _numItems = 100;
                    int value = 0;
                    for (int i = 0; i < _numItems; i++)
                    {
                        value = (startYear + i);
                        AddItem(value.ToString(), value);
                    }
                    _currItemIndex = _numItems - 20;
                }
                break;

            case DataType.Month:
                {
                    _numItems = 12;
                    int value = 0;
                    for (int i = 0; i < _numItems; ++i)
                    {
                        value = (1 + i);
                        AddItem(string.Format("{0}月", value), value);
                    }
                }
                break;

            case DataType.Day:
                {
                    _numItems = 31;
                    int value = 0;
                    for (int i = 0; i < _numItems; i++)
                    {
                        value = (1 + i);
                        AddItem(value.ToString(), value);
                    }
                }
                break;
        }

        _numUIs = _uis.Length;

        RefreshDisplayTexts();

        if (_datepicker != null)
        {
            UIEventListener listener = UIEventListener.Get(_datepicker.gameObject);
            if (listener != null)
            {
                listener._onBeginDrag = (data) =>
                {
                    //Debug.Log("===> _onBeginDrag");
                };

                listener._onDrag = (data) =>
                {
                    //Debug.Log("===> _onDrag");
                    if (_pickerContainer)
                    {
                        Vector2 pos = _pickerContainer.anchoredPosition;
                        pos.y += (data.delta.y / 2.0f);
                        _pickerContainer.anchoredPosition = pos;
                        UpdatePickerPos();
                    }
                };

                listener._onEndDrag = (data) =>
                {
                    //Debug.Log("===> _onEndDrag");
                    if (_pickerContainer)
                    {
                        Vector2 pos = _pickerContainer.anchoredPosition;
                        if (pos.y >= (_uiItemHeight * 1.5f))
                        {
                            _lerping = true;
                            _targetY = _uiItemHeight;
                        }
                        else if (pos.y >= (_uiItemHeight * 0.5f))
                        {
                            _lerping = true;
                            _targetY = _uiItemHeight;
                        }
                        else if (pos.y <= -(_uiItemHeight * 1.5f))
                        {
                            _lerping = true;
                            _targetY = -_uiItemHeight;
                        }
                        else if (pos.y <= -(_uiItemHeight * 0.5f))
                        {
                            _lerping = true;
                            _targetY = -_uiItemHeight;
                        }
                        else
                        {
                            _lerping = true;
                            _targetY = 0f;
                        }
                    }
                };
            }
        }
    }

    public void Setup(List<PickerItem> items, int itemIndex)
    {
        _currItemIndex = itemIndex;
        _items = items;
        _numItems = _items.Count;

        RefreshDisplayTexts();
    }

    private void RefreshDisplayTexts()
    {
        int itemIndex = _currItemIndex - Mathf.FloorToInt(_numUIs / 2);
        itemIndex = CalcLoopIndex(itemIndex, 0, _numItems);

        for (int i = 0; i < _numUIs; i++)
        {
            itemIndex = CalcLoopIndex(itemIndex, 0, _numItems);
            _uis[i].text = _items[itemIndex]._displayText;
            itemIndex++;
        }
    }

    private void Update()
    {
        UpdatePickerPos();

        if (_lerping)
        {
            Vector2 pos = _pickerContainer.anchoredPosition;
            float lerpValue = 0.05f; // Time.deltaTime * 2f;
            if (Mathf.Abs(pos.y - _targetY) < 2f)
            {
                lerpValue = 1f;
                _lerping = false;
            }
            pos.y = Mathf.Lerp(pos.y, _targetY, lerpValue);
            _pickerContainer.anchoredPosition = pos;
        }
    }

    private void UpdatePickerPos()
    {
        if (_pickerContainer)
        {
            Vector2 pos = _pickerContainer.anchoredPosition;
            float ITEM_H = _uiItemHeight;
            float OVER_H = ITEM_H / 2f;
            if (pos.y > OVER_H || pos.y < -OVER_H)
            {
                int overItemCounts = 0;
                if (pos.y > 0f)
                {
                    overItemCounts = Mathf.RoundToInt(pos.y / ITEM_H);
                }
                else
                {
                    overItemCounts = Mathf.FloorToInt(pos.y / ITEM_H);
                }
                
                if (overItemCounts != 0)
                {
                    float overH = overItemCounts * ITEM_H;

                    _currItemIndex += overItemCounts;
                    _currItemIndex = CalcLoopIndex(_currItemIndex, 0, _numItems);

                    RefreshDisplayTexts();

                    pos.y -= overH;
                    _pickerContainer.anchoredPosition = pos;

                    OnCurrItemIndexChanged();

                    //Debug.Log("new value=" + _items[_currItemIndex]._displayText);
                }
            }
        }
    }

    public void AddItem(string displayText, int value)
    {
        if (_items == null)
        {
            _items = new List<PickerItem>();
        }

        _items.Add(new PickerItem { _displayText = displayText, _value = value });
    }

    public PickerItem Get(int index)
    {
        if (index >= 0 && index < _items.Count)
        {
            return _items[index];
        }
        return null;
    }

    public string GetCurrDisplayText()
    {
        return string.Empty;
    }

    public int GetCurrValue()
    {
        return _items[_currItemIndex]._value;
    }

    public void SetOnValueChangedCallback(System.Action<int> callback)
    {
        _onValueChangedCallback = callback;
    }

    private int CalcLoopIndex(int index, int min, int max)
    {
        index %= max;
        index = (index < min) ? (max + index) : index;
        return index;
    }

    private void OnCurrItemIndexChanged()
    {
        if (_onValueChangedCallback != null)
        {
            _onValueChangedCallback(_items[_currItemIndex]._value);
        }
    }
}
