﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatepickerCtrl : MonoBehaviour
{
    [SerializeField] private PickerCtrl _yearPickerCtrl;
    [SerializeField] private PickerCtrl _monthPickerCtrl;
    [SerializeField] private PickerCtrl _dayPickerCtrl;

    private void Awake()
    {
        if (_yearPickerCtrl != null)
        {
            _yearPickerCtrl.SetOnValueChangedCallback(OnYearChanged);
        }

        if (_monthPickerCtrl != null)
        {
            _monthPickerCtrl.SetOnValueChangedCallback(OnMonthChanged);
        }

        if (_dayPickerCtrl != null)
        {
            _dayPickerCtrl.SetOnValueChangedCallback(OnDayChanged);
        }
    }

    public int GetYear()
    {
        if (_yearPickerCtrl != null)
        {
            return _yearPickerCtrl.GetCurrValue();
        }
        return 0;
    }

    public int GetMonth()
    {
        if (_monthPickerCtrl != null)
        {
            return _monthPickerCtrl.GetCurrValue();
        }
        return 0;
    }

    public int GetDay()
    {
        if (_dayPickerCtrl != null)
        {
            return _dayPickerCtrl.GetCurrValue();
        }
        return 0;
    }

    private void OnYearChanged(int year)
    {
        ResetDayPicker();
    }

    private void OnMonthChanged(int month)
    {
        ResetDayPicker();
    }

    private void OnDayChanged(int day)
    {

    }

    private void ResetDayPicker()
    {
        if (_dayPickerCtrl != null)
        {
            int days = System.DateTime.DaysInMonth(GetYear(), GetMonth());
            List<PickerCtrl.PickerItem> items = new List<PickerCtrl.PickerItem>();
            int value = 0;
            for (int i = 0; i < days; i++)
            {
                value = (1 + i);
                items.Add(new PickerCtrl.PickerItem { _displayText = value.ToString(), _value = value });
            }

            _dayPickerCtrl.Setup(items, 0);
        }
    }
}
