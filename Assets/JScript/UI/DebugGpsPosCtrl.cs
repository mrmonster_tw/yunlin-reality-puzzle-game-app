﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGpsPosCtrl : MonoBehaviour
{
    [SerializeField] private float _mapLat = 0f;
    [SerializeField] private float _mapLon = 0f;
    [SerializeField] private float _posLat = 0f;
    [SerializeField] private float _posLon = 0f;
    [SerializeField] private bool _asGrid = false;

    private float _lastUpdateTime = 0f;
    private RectTransform _rectTrans;

    void Start() {
        _rectTrans = GetComponent<RectTransform>();

        _mapLat = 23.52f;
        _mapLon = 120.26f;
        _posLat = 23.567361f;
        _posLon = 120.304694f;
    }

    void Update() {
        if (Time.unscaledTime >= _lastUpdateTime + 1f) {
            _lastUpdateTime = Time.unscaledTime;

            const int GRID_W = 30;
            const int GRID_H = 30;

            int posX = -250;
            int posY = -250;

            float diffLat = _posLat - _mapLat;
            float diffLon = _posLon - _mapLon;

            if (_asGrid) {
                posY += Mathf.FloorToInt(diffLat / 0.01f) * GRID_H;
                posX += Mathf.FloorToInt(diffLon / 0.005f) * GRID_W;
            }
            else {
                posY += Mathf.FloorToInt(diffLat * ((float)GRID_H / 0.01f));
                posX += Mathf.FloorToInt(diffLon * ((float)GRID_W / 0.005f));
            }

            _rectTrans.anchoredPosition = new Vector2(posX, posY);

        /*
        經度(Latitude)每差 0.01 大約實際距離是 1.024 公里
        緯度(Longitude)每差 0.01 大約實際距離是 1.112 公里 
        */

        /*
        Lat:0.01f 大概 1110m
        Lon:0.005f 大概 511m
        */

        /*
        float la = aa;
        float lo = bb;

        for (int x = 0; x < 17; x++)
        {
            for (int y = 0; y < 19; y++)
            {
                point_cn = Instantiate(point, xx.transform);
                point_cn.transform.localPosition = new Vector3(posx, posy);
                posy = posy + 30;

                point_cn.GetComponent<point_SC>().latitude = la;
                point_cn.GetComponent<point_SC>().longitude = lo;
                la = la + 0.01f;
            }

            posx = posx + 30;
            posy = -250;
            la = aa;
            lo = lo + 0.005f;
        }
        posx = -250;
        posy = -250;
        */
        }
    }
}
