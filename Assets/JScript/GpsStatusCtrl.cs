﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GpsStatusCtrl : MonoBehaviour
{
    public enum GameGpsStatus {
        None,
        Failed,
        Stopped,
        Initializing,
        Weak,
        Ok
    }

    [SerializeField] private GameObject _contents;
    [SerializeField] private Text _textStatus;
    [SerializeField] private float _desiredAccuracy = 70;
    [SerializeField] private float _updateDistance = 0.1f;
    [SerializeField] private Animator _animator;
    [SerializeField] private AnimationClip _animationClip;
    [SerializeField] private GPS _gps;

    private bool _gpsInitializing = false;
    private string _gpsStatusText = string.Empty;
    private GameGpsStatus _gameGpsStatus = GameGpsStatus.None;
    private System.Action _gpsReadyCallback;

    void Awake() {
        _textStatus.text = string.Empty;
        Hide();
    }

    public void Show(System.Action gpsReadyCallback) {
        Debug.LogFormat("<color=yellow>GpsStatusCtrl.Show</color>");

        _gpsReadyCallback = gpsReadyCallback;
        _contents.SetActive(true);
        _textStatus.text = "檢查GPS訊號";
        ResetPos();
        StartCoroutine(StartGPS());
        PlayAnimSlideIn();

        _gps.SetRunning(true);
    }

    public void Hide() {
        _contents.SetActive(false);
        _gps.SetRunning(false);
    }

    public GameGpsStatus GetGpsStatus() {
        return _gameGpsStatus;
    }

    private void ResetPos() {
        _contents.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 1350);
    }

    IEnumerator StartGPS() {
    #if DEBUG_LOG
        Debug.Log("StartGPS()");
    #endif

        yield return new WaitForEndOfFrame();
        Debug.LogFormat("<color=yellow>2 Ask GPS Start!</color>");
        Input.location.Start(_desiredAccuracy, _updateDistance);
        _gpsInitializing = true;

        while (true) {
            CheckGpsStatus();

            switch (_gameGpsStatus) {
                case GameGpsStatus.Failed:
                    _textStatus.text = "GPS未開啟\n請再次確認[定位]有開啟";
                break;

                case GameGpsStatus.Stopped:
                    _textStatus.text = "無法使用GPS\n請再次確認[定位]有開啟";
                break;

                case GameGpsStatus.Initializing:
                    _textStatus.text = "取得GPS訊號中\n若等待超過10秒,可能[定位]未開啟或是開啟失敗";
                break;

                case GameGpsStatus.Weak:
                    _textStatus.text = "微弱的GPS訊號\n請移動到比較無遮蔽的位置";
                break;

                case GameGpsStatus.Ok:
                    _textStatus.text = "GPS訊號正常";
                    yield return new WaitForSeconds(1.5f);
                    PlayAnimSlideOut();
                    yield break;
                break;
            }

            yield return new WaitForSeconds(1f);
        }
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.A)) {
            Show(null);
            PlayAnimSlideIn();
        }
        else if (Input.GetKeyDown(KeyCode.S)) {
            Show(null);
            PlayAnimSlideOut();
        }
    }

    void CheckGpsStatus() {

        switch (Input.location.status) {
            case LocationServiceStatus.Failed:
                _gameGpsStatus = GameGpsStatus.Failed;
            break;

            case LocationServiceStatus.Stopped:
                _gameGpsStatus = GameGpsStatus.Stopped;
            break;

            case LocationServiceStatus.Initializing:
                _gameGpsStatus = GameGpsStatus.Initializing;
            break;

            case LocationServiceStatus.Running:
                if (Input.location.lastData.horizontalAccuracy > _desiredAccuracy) {
                    _gameGpsStatus = GameGpsStatus.Weak;
                } else {
                    _gameGpsStatus = GameGpsStatus.Ok;
                }
            break;
        }
    }

    public void PlayAnimSlideIn() {
        _animator.Play("SlideIn");
    }

    public void PlayAnimSlideOut() {
        _animator.Play("SlideOut");
    }
}
