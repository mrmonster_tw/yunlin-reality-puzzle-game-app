﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusCtrl : MonoBehaviour
{
    [SerializeField] private GameObject _gameObjBig;
    [SerializeField] private GameObject _gameObjSmall;

    public enum BonusType {
        Big,
        Small,
        All
    }

    // Start is called before the first frame update
    void Start()
    {
        Show(BonusType.All, false);
    }

    public void Show(BonusType type, bool show) {
        switch (type) {
            case BonusType.Big:
                if (_gameObjBig != null) {
                    _gameObjBig.SetActive(show);
                }
                break;
            case BonusType.Small:
                if (_gameObjSmall != null) {
                    _gameObjSmall.SetActive(show);
                }
                break;
            case BonusType.All:
                Show(BonusType.Big, show);
                Show(BonusType.Small, show);
                break;
        }
    }
}
