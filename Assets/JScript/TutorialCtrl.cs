﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TutorialCtrl : MonoBehaviour
{
    [SerializeField] private VideoPlayer _videoPlayer;
    [SerializeField] private GameObject _gameObjContent;
    [SerializeField] private VideoClip[] _videoClips;
    [SerializeField] private GameObject _gameObjContinue;

    private int _currVideoClipIndex = -1;
    private float _showContinueDelay = 0f;

    private void Awake() {
        Show(false);

        _videoPlayer.loopPointReached += OnVideoEnded;

        HideContinue();
    }

    public void Show(bool show) {
        Debug.Log("show=" + show);
        if (_gameObjContent != null) {
            _gameObjContent.SetActive(show);
            if (_videoPlayer != null) {
                if (show) {
                    _currVideoClipIndex = -1;
                    PlayNextVideo();
                }
                else {
                    _videoPlayer.Stop();
                }
            }
        }
    }

    private void Update() {
        if (_showContinueDelay > 0f) {
            if (Time.unscaledTime >= _showContinueDelay + 2f) {
                _showContinueDelay = 0f;
                ShowContinue();
            }
        }
    }

    private void PlayNextVideo() {
        if (_videoClips != null) {
            _currVideoClipIndex++;
            if (_currVideoClipIndex >= 0 && _currVideoClipIndex < _videoClips.Length) {
                _videoPlayer.clip = _videoClips[_currVideoClipIndex];
                _videoPlayer.frame = 0;
                _videoPlayer.Play();

                // _showContinueDelay = Time.unscaledTime;
            }
        }
    }

    private bool HasNextVideo() {
        if (_videoClips != null) {
            int newIndex = _currVideoClipIndex + 1;
            if (newIndex >= 0 && newIndex < _videoClips.Length) {
                return true;
            }
        }
        return false;
    }

    private void OnVideoEnded(VideoPlayer source) {
        ShowContinue();
    }

    public void OnContinueClicked() {
        HideContinue();

        if (HasNextVideo()) {
            PlayNextVideo();
        }
        else {
            Show(false);
        }
    }

    private void ShowContinue() {
        if (_gameObjContinue != null) {
            _gameObjContinue.SetActive(true);
        }
    }

    private void HideContinue() {
        if (_gameObjContinue != null) {
            _gameObjContinue.SetActive(false);
        }
    }
}
