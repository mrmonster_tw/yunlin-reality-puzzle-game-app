﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMoveGpsPanelCtrl : MonoBehaviour
{
	[SerializeField] private GameObject _contentRoot;

	private bool _directionShowing = false;
    private Vector2 _playerPos = Vector2.zero;

    public bool IsDirectionShowing() { return _directionShowing; }

	public void OnDebugMoveGpsClicked()
	{
		_directionShowing = !_directionShowing;
		_contentRoot.SetActive(_directionShowing);
        if (_directionShowing)
        {
            ResetPlayerPos();
        }
	}

    public void OnDirectionClicked(string direction)
	{
        const float SHIFT_X = 15f;
        const float SHIFT_Y = 15f;

        if (direction == "up")
        {
            _playerPos.y += SHIFT_Y;
        }
        else if (direction == "down")
        {
            _playerPos.y += -SHIFT_Y;
        }
        else if (direction == "right")
        {
            _playerPos.x += SHIFT_X;
        }
        else if (direction == "left")
        {
            _playerPos.x += -SHIFT_X;
        }
    }

    private void ResetPlayerPos()
    {
        _playerPos = new Vector2(-10, 80);
    }

    public Vector2 GetPlayerPos()
    {
        return _playerPos;
    }
}
