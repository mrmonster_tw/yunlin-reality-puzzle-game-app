﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupDialogCtrl : MonoBehaviour
{
	[SerializeField] private Text _textBody;
    [SerializeField] private GameObject _buttonOk;
    [SerializeField] private GameObject _buttonOkRight;
    [SerializeField] private GameObject _buttonCancel;

    public static PopupDialogCtrl _instance;

    //private System.Action _confirmClickedCallback;
    private float _duration = 0f;
	private float _startedTime = 0f;
    private System.Action _timeupCallback;
    private System.Action _okCallback;
    private System.Action _cancelCallback;

    private void Awake()
    {
        if (_instance != null)
		{
			Destroy(this);
			return;
		}
		_instance = this;
	}

	private void OnDestroy()
	{
        Debug.Log("PopupDialogCtrl.OnDestroy");
		if (_instance == this)
		{
            Debug.Log(" 0");
			_instance = null;
		}
	}

    private void Start()
    {
        Hide(); // Hide in default.
    }

    private void Update()
	{
        if (_duration > 0f)
		{
            if (Time.realtimeSinceStartup >= _startedTime + _duration)
            {
                if (_timeupCallback == null) {
                    Hide();
                }
                else {

                }
                _duration = 0f;
            }
        }
	}

	//public void SetConfirmClickedCallback(System.Action callback)
	//{
	//	_confirmClickedCallback = callback;
	//}

	//public void OnConfirmClicked()
	//{
    //  if (_confirmClickedCallback != null)
	//	{
	//		_confirmClickedCallback();
	//	}
	//}

    public void Setup(string body)
    {
        if (_textBody != null)
        {
            _textBody.text = body;
        }
    }

    public void Show(float duration = 0f)
	{
        Reset();

		gameObject.SetActive(true);
        if (duration > 0f)
        {
            _duration = duration;
            _startedTime = Time.realtimeSinceStartup;
        }
	}

    public void Show(string bodyText, float duration = 0f, System.Action timeupCallback = null)
	{
        Reset();

		gameObject.SetActive(true);
        _duration = duration;
        _startedTime = Time.realtimeSinceStartup;

        Setup(bodyText);
        _timeupCallback = timeupCallback;
	}

    public void Show(string bodyText, System.Action okCallback) {
        Reset();

		gameObject.SetActive(true);

        Setup(bodyText);
        _okCallback = okCallback;
        _buttonOk.SetActive(true);
    }

    public void Show(string bodyText, System.Action okCallback, System.Action cancelCallback) {
        Reset();

		gameObject.SetActive(true);
        
        Setup(bodyText);
        _okCallback = okCallback;
        _cancelCallback = cancelCallback;
        _buttonOkRight.SetActive(true);
        _buttonCancel.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void OnOkClicked() {
        if (_okCallback != null) {
            System.Action callbackCache = _okCallback;
            _okCallback = null;
            callbackCache();
        }
        else {
            Hide();
        }
    }

    public void OnCancelClicked() {
        if (_cancelCallback != null) {
            System.Action callbackCache = _cancelCallback;
            _cancelCallback = null;
            callbackCache();
        }
        else {
            Hide();
        }
    }

    private void Reset() {
        _okCallback = null;
        _buttonOk.SetActive(false);
        _buttonOkRight.SetActive(false);

        _cancelCallback = null;
        _buttonCancel.SetActive(false);

        _timeupCallback = null;
    }
}
