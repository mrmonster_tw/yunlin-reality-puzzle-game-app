﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer : MonoBehaviour
{
    public GameObject answer;
    public GameObject right;
    public GameObject wrong;

    public GameObject mark;
    public GameObject Axe;
    public GameObject Axe_q;
    public GameObject self;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void check_answer(GameObject xx)
    {
        answer = xx;
    }
    public void send_answer(GameObject yy)
    {
        string questionPrefix = QuestionPanelCtrl.ParseQuestionName(self.name);
        if (!PlayerDataCtrl._instance.HasOpenedQuestionName(questionPrefix))
        {
            Debug.Log("Not has questionPrefix=" + questionPrefix);
            PlayerDataCtrl._instance.AddOpenedQuestionName(questionPrefix);
            AppWebService._instance.RequestOpenedQuestionNew(questionPrefix, null, null);
        }

        if (answer == yy)
        {
            right.SetActive(true);
            //mark.SetActive(true);
            //Axe_q.SetActive(false);
            //Axe.GetComponent<BoxCollider2D>().enabled = false;
            self.SetActive(false);

            PlayerDataCtrl._instance.AddScoreName(self.name);
        }
        else
        {
            wrong.SetActive(true);
        }
    }
}
