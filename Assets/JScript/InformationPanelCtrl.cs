﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ind.leoyintw.utils;

public class InformationPanelCtrl : MonoBehaviour
{
    [SerializeField] GameObject _contents;
    [SerializeField] GameObject[] _correctAnswers;
    [SerializeField] private BonusListCtrl _bonusListCtrl;

    private Dictionary<int, List<string>> _answerCatalogs = new Dictionary<int, List<string>> {
        { 0, new List<string>{ "成龍溼地", "遊客中心", "好蝦冏男社" } },
        { 1, new List<string>{ "烏克麗麗", "王堡興業", "水林小陳", "顏思齊" } },
        { 2, new List<string>{ "北港老街", "雲林金水" } }
    };

    private NoticeObserverComponent _noticeObserverComp;

    private void Awake()
    {
        _noticeObserverComp = gameObject.AddComponent<NoticeObserverComponent>();
        _noticeObserverComp._noticeObserver.Add((int)GameNotices.ScoreNameAdded, HandleScoreNameAdded);
	}

    private void Start()
    {
        RefreshAnswerMarks();
    }

    public void Show(bool show)
    {
        if (_contents != null)
        {
            _contents.SetActive(show);
            if (show)
            {
                RefreshAnswerMarks();
            }
        }
    }

    public bool IsContentsShowing()
    {
        return _contents.activeSelf;
    }

    private void ResetAnswerMarks()
    {
        foreach (var mark in _correctAnswers)
        {
            mark.SetActive(false);
        }
    }

    private void RefreshAnswerMarks()
    {
        ResetAnswerMarks();

        Dictionary<int, int> locationScores = new Dictionary<int, int>();
        List<string> scoreNames = PlayerDataCtrl._instance.GetScoreNames();
        foreach (var scoreName in scoreNames)
        {
            string[] parameters = scoreName.Split('_');
            int locationIndex = ScoreNameToLocationIndex(parameters[0]);
            if (locationIndex >= 0)
            {
                if (!locationScores.ContainsKey(locationIndex))
                {
                    locationScores.Add(locationIndex, 0);
                }
                locationScores[locationIndex]++;
            }
        }

        foreach (var pair in locationScores)
        {
            SetNumCorrectAnswers(pair.Key, pair.Value);
        }
    }

    private int ScoreNameToLocationIndex(string scoreName)
    {
        foreach (var pair in _answerCatalogs)
        {
            for (int i = 0; i < pair.Value.Count; ++i)
            {
                if (pair.Value[i] == scoreName)
                {
                    return pair.Key;
                }
            }
        }
        return -1;
    }

    public void SetNumCorrectAnswers(int locationIndex, int numCorrectAnswers)
    {
        // locationIndex = 0:口湖 1:水林 2:北港
        if (locationIndex >= 0 && locationIndex < 3)
        {
            const int NUM_ANSWER_PER_LOCATION = 4;

            int answerIndex = locationIndex * NUM_ANSWER_PER_LOCATION;
            for (int i = 0; i < numCorrectAnswers; i++)
            {
                if (answerIndex + i < _correctAnswers.Length)
                {
                    _correctAnswers[answerIndex + i].SetActive(true);
                }
            }
        }
    }

    private void OnScoreNameAdded(string scoreName)
    {
        AppWebService._instance.RequestPlayerScoreNew(
            scoreName, null, null);

        RefreshAnswerMarks();
    }

    public void OnBonusClicked() {
        if (_bonusListCtrl != null) {
            _bonusListCtrl.Show(true);
        }
    }

    private void HandleScoreNameAdded(object dataObj) {
        string scoreName = (string)dataObj;
        AppWebService._instance.RequestPlayerScoreNew(scoreName, null, null);
        RefreshAnswerMarks();
    }
}
