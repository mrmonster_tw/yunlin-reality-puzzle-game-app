﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlbumsCtrl : MonoBehaviour
{
    [SerializeField] private GameObject _contents;
    [SerializeField] private Image _imageTitle;
    [SerializeField] private Image[] _images;
    [SerializeField] private  Text[] _textPhotoNames;
    [SerializeField] private Sprite[] _titleSprites;
    [SerializeField] private Sprite[] _defaultPhotoSprites;

    private string[] _photoNames = new string[] {
        "朝天宮", "振興戲院", "復興鐵橋"
        , "黃金蝙蝠生態館", "七角井", "李萬起故居"
        , "成龍濕地", "口湖遊客中心", "宜梧滯洪池"
    };
    private Sprite[] _photoSprites;

    private void Start() {
        Show(false);    // hide in default.
    }

    public void Show(bool show) {
        if (_contents != null) {
            _contents.SetActive(show);
        }
    }

    public void Setup(int albumIndex) {
        if (_imageTitle != null && _titleSprites != null && _titleSprites.Length == 3) {
            _imageTitle.sprite = _titleSprites[albumIndex];
        }

        if (_images != null && _images.Length == 3) {
            if (_defaultPhotoSprites != null && _defaultPhotoSprites.Length == 3 * 3) {
                _images[0].sprite = _defaultPhotoSprites[(albumIndex * 3) + 0];
                _images[1].sprite = _defaultPhotoSprites[(albumIndex * 3) + 1];
                _images[2].sprite = _defaultPhotoSprites[(albumIndex * 3) + 2];
            }
        }

        if (_textPhotoNames != null && _textPhotoNames.Length == 3) {
            if (_photoNames != null && _photoNames.Length == 3 * 3) {
                _textPhotoNames[0].text = _photoNames[(albumIndex * 3) + 0];
                _textPhotoNames[1].text = _photoNames[(albumIndex * 3) + 1];
                _textPhotoNames[2].text = _photoNames[(albumIndex * 3) + 2];
            }
        }
    }
}
