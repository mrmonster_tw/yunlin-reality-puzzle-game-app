﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class QuestionSettingCtrl : MonoBehaviour
{
    [System.Serializable]
    public class Question
	{
        public string name;
		public string question;
		public string[] selections;
	}

	[System.Serializable]
	public class QuestionSetting
	{
		public Question[] questions;
	}

    public static QuestionSettingCtrl _instance;

    private QuestionSetting _questionSetting;    

    private void Awake()
    {
		if (_instance != null)
		{
			Destroy(this);
			return;
		}
		_instance = this;
	}

	private void OnDestroy()
	{
        Debug.Log("QuestionSettingCtrl.OnDestroy");
		if (_instance == this)
		{
            Debug.Log(" 0");
			_instance = null;
		}
	}

    void Start()
    {
        ReadFromFile();        
    }

    public Question GetQuestion(string name)
    {
        foreach (var question in _questionSetting.questions)
        {
            if (question.name == name)
            {
                return question;
            }
        }
        return null;
    }

    public Question GetQuestion(string mark, int number)
    {
        return GetQuestion(string.Format("{0}_{1}", mark, number));
    }

    private void ReadFromFile()
	{
        string fileName = "ques.data";
        string filePath = Application.streamingAssetsPath + "/" + fileName;
        if (File.Exists(filePath))
        {
            Debug.Log("File in local.");
        }
        else
        {
            Debug.LogError("Cannot find file! named" + fileName);
        }

        string dataAsJson;
        #if UNITY_EDITOR || UNITY_IOS
        dataAsJson = File.ReadAllText(filePath);

#elif UNITY_ANDROID
        WWW reader = new WWW (filePath);
        while (!reader.isDone) {
        }
        dataAsJson = reader.text;
#endif
        Debug.Log("dataAsJson=" + dataAsJson);

        StartCoroutine(Read());
	}

    private IEnumerator Read()
	{
        yield return new WaitForEndOfFrame();

        string fileName = "ques.data";
        string filePath = Application.streamingAssetsPath + "/" + fileName;

        string dataJson = string.Empty;
#if UNITY_EDITOR || UNITY_IOS
        dataJson = File.ReadAllText(filePath);
#elif UNITY_ANDROID
        WWW data = new WWW(filePath);
        yield return data;

		if (string.IsNullOrEmpty(data.error))
		{
            dataJson = data.text;
        }
        else
        {
            Debug.LogError("ERR: " + data.error);
        }
#endif

        if (!string.IsNullOrEmpty(dataJson))
        {
            _questionSetting = JsonUtility.FromJson<QuestionSetting>(dataJson);
            Debug.Log("_questionSetting json=" + JsonUtility.ToJson(_questionSetting));
        }
	}
}
