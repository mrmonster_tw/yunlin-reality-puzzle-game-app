﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ind.leoyintw.utils;

public class Main : MonoBehaviour
{
    [SerializeField] private Button _buttonLocationOne;
    [SerializeField] private Text _textBirthday;
    [SerializeField] private DatepickerCtrl _datepickerCtrl;
    [SerializeField] private GameObject _inputBirthdayPanel;

    [SerializeField] private InputField _inputAccount;
    [SerializeField] private InputField _inputPassword;

    [SerializeField] private InputField _inputRegName;    
    [SerializeField] private Text _textRegSex;
    [SerializeField] private Text _textRegLocation;
    [SerializeField] private InputField _inputRegUsername;
    [SerializeField] private InputField _inputRegPassword;
    [SerializeField] private RegisterUiCtrl _registerUiCtrl;
    [SerializeField] private GameObject _gameObjHomepage;
    [SerializeField] private GameObject _prefabDebugGpsPanel;
    [SerializeField] private GameObject _canvas;
    [SerializeField] private Text _textVersion;
    [SerializeField] private TutorialCtrl _tutorialCtrl;
    [SerializeField] private DialogConfirmLeaveCtrl _dialogConfirmLeaveCtrl;
    [SerializeField] private Text _textGotScore;
    [SerializeField] private Text _textRemainScore;
    [SerializeField] private GpsStatusCtrl _gpsStatusCtrl;
    [SerializeField] private GameObject _imageDebugGpsPos;

    private const string PLAYERPREFS_KEY_NoticeRequiredPermissions = "NoticeRequiredPermissions";
    
    public GameObject login;
    public GameObject regi;
    public GameObject regi_su;
    public GameObject regi_fail;

    public GameObject map;
    public InformationPanelCtrl _informationPanelCtrl;
    public GameObject player_pos;
    public GameObject can;

    public GameObject point;
    public GameObject parte_1;
    public GameObject parte_2;
    public GameObject parte_3;
    public GameObject parte_4;

    public GameObject 口湖鄉;
    public GameObject 水林鄉;
    public GameObject 北港鎮;
    public GameObject 四湖鄉;
    float posx = -250;
    float posy = -250;

    public GameObject[] pointlist;
    GameObject point_cn;

    public int q_ramdon = 0;

    public InputField name;
    public Text sex;
    public Text name_1;
    public Text sex_1;

    public GameObject boy_pic;
    public GameObject girl_pic;

    private DebugMoveGpsPanelCtrl _debugMoveGpsPanelCtrl;
    private Dictionary<string, bool> _mapObjShowing = new Dictionary<string, bool>();

    private float _dialogStartedTime = 0f;
    private float _dialogMinDuration = 0f;
    private string _cachedSucceedRespText = string.Empty;
    private System.Action<string> _succeedRespHandler = null;
    private NoticeObserverComponent _noticeObserverComp;
    private string _googleMapCoord = string.Empty;

    void Awake() {

    }

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        if (_buttonLocationOne != null)
        {
            _buttonLocationOne.onClick.AddListener(() =>
            {
                Debug.Log("001");
            });
        }
#endif

        q_ramdon = PlayerPrefs.GetInt("q_ramdon");

        point_fn(23.50f, 120.135f, parte_1);
        point_fn(23.50f, 120.195f, parte_2);
        point_fn(23.52f, 120.26f, parte_3);
        point_fn(23.55f, 120.145f, parte_4);

        if (q_ramdon == 0)
        {
            q_ramdon = Random.Range(1, 3);
            PlayerPrefs.SetInt("q_ramdon", q_ramdon);
        }

        if (_textVersion != null)
        {
            _textVersion.text = string.Format("V{0}", Application.version);
        }

        if (_inputBirthdayPanel != null)
        {
            _inputBirthdayPanel.SetActive(false);
        }

        // Hide in default
        if (_debugMoveGpsPanelCtrl != null)
        {
            _debugMoveGpsPanelCtrl.gameObject.SetActive(false);
        }

        _mapObjShowing.Add("map", false);
        _mapObjShowing.Add("口湖鄉", false);
        _mapObjShowing.Add("水林鄉", false);
        _mapObjShowing.Add("北港鎮", false);

        _noticeObserverComp = gameObject.AddComponent<NoticeObserverComponent>();
        _noticeObserverComp._noticeObserver.Add((int)GameNotices.ScoreNameAdded, (dataObj) => RefreshScoreStatus());
        _noticeObserverComp._noticeObserver.Add((int)GameNotices.ScoreChanged, (dataObj) => RefreshScoreStatus());

        if (PlayerPrefs.GetInt(PLAYERPREFS_KEY_NoticeRequiredPermissions, 0) == 0) {
            PopupDialogCtrl._instance.Show(
                "完整的遊戲體驗會需要下列權限\n[<color=lime>GPS位置</color>]用於觸發遊戲地圖上的景點",
                () => {
                    PlayerPrefs.SetInt(PLAYERPREFS_KEY_NoticeRequiredPermissions, 1);
                    PlayerPrefs.Save();
                    PopupDialogCtrl._instance.Hide();

                    CheckAndProcessAutoLogin();
                }
            );
        }
        else {
            CheckAndProcessAutoLogin();
        }

        RefreshScoreStatus();

#if DEBUG_LOG
        if (_prefabDebugGpsPanel != null && _canvas != null)
        {
            GameObject gpsPanel = Instantiate(_prefabDebugGpsPanel);
            _debugMoveGpsPanelCtrl = gpsPanel.GetComponent<DebugMoveGpsPanelCtrl>();
            gpsPanel.transform.SetParent(_canvas.transform, false);

            GPS gps = gameObject.GetComponent<GPS>();
            if (gps != null) {
                gps._debugMoveGpsPanelCtrl = _debugMoveGpsPanelCtrl;
            }
        }
#endif

#if !DEBUG_LOG
        GameObject gameObjReporter = GameObject.Find("Reporter");
        if (gameObjReporter != null) {
            Destroy(gameObjReporter);
            gameObjReporter = null;
        }
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (口湖鄉.activeSelf)
        {
            player_pos.transform.SetParent(parte_1.transform);
        }
        if (水林鄉.activeSelf)
        {
            player_pos.transform.SetParent(parte_2.transform);
        }
        if (北港鎮.activeSelf)
        {
            player_pos.transform.SetParent(parte_3.transform);
        }
        if (四湖鄉.activeSelf)
        {
            player_pos.transform.SetParent(parte_4.transform);
        }

        pointlist = GameObject.FindGameObjectsWithTag("pos");

#if DEV
        UpdateDebugMoveGpsPanel();
#endif
        UpdateDialog();
    }

    private void CheckAndProcessAutoLogin() {
        if (AppWebService._instance != null && AppWebService._instance.HasToken())
        {
            ProcessAutoLogin();
        }
    }

    public void OnEnterMapClicked(int mapID) {
        _gpsStatusCtrl.Show(null);
    }

    public void OnLeaveMapClicked(int mapID) {
        _gpsStatusCtrl.Hide();
    }

    public void OnOpenNavMapClicked(string coord) {
        _googleMapCoord = coord;

        PopupDialogCtrl._instance.Show("即將離開APP\n並開啟google地圖",
            () => {
                string url = string.Format("{0}{1}/", "https://www.google.com/maps/place/", _googleMapCoord);
                Application.OpenURL(url);
                PopupDialogCtrl._instance.Hide();
            }, 
            () => PopupDialogCtrl._instance.Hide()
        );
    }

    private void UpdateDebugMoveGpsPanel()
    {
        if (_debugMoveGpsPanelCtrl != null)
        {
            GameObject currGameObj = null;

            currGameObj = map;
            if (_mapObjShowing.ContainsKey(currGameObj.name))
            {
                if (_mapObjShowing[currGameObj.name] != currGameObj.activeSelf)
                {
                    _mapObjShowing[currGameObj.name] = currGameObj.activeSelf;
                    if (_mapObjShowing[currGameObj.name])
                    {
                        _debugMoveGpsPanelCtrl.gameObject.SetActive(!_mapObjShowing[currGameObj.name]);
                    }
                }
            }

            currGameObj = 口湖鄉;
            if (_mapObjShowing.ContainsKey(currGameObj.name))
            {
                if (_mapObjShowing[currGameObj.name] != currGameObj.activeSelf)
                {
                    _mapObjShowing[currGameObj.name] = currGameObj.activeSelf;
                    if (_mapObjShowing[currGameObj.name])
                    {
                        _debugMoveGpsPanelCtrl.gameObject.SetActive(_mapObjShowing[currGameObj.name]);
                    }
                }
            }

            currGameObj = 水林鄉;
            if (_mapObjShowing.ContainsKey(currGameObj.name))
            {
                if (_mapObjShowing[currGameObj.name] != currGameObj.activeSelf)
                {
                    _mapObjShowing[currGameObj.name] = currGameObj.activeSelf;
                    if (_mapObjShowing[currGameObj.name])
                    {
                        _debugMoveGpsPanelCtrl.gameObject.SetActive(_mapObjShowing[currGameObj.name]);
                    }
                }
            }

            currGameObj = 北港鎮;
            if (_mapObjShowing.ContainsKey(currGameObj.name))
            {
                if (_mapObjShowing[currGameObj.name] != currGameObj.activeSelf)
                {
                    _mapObjShowing[currGameObj.name] = currGameObj.activeSelf;
                    if (_mapObjShowing[currGameObj.name])
                    {
                        _debugMoveGpsPanelCtrl.gameObject.SetActive(_mapObjShowing[currGameObj.name]);
                    }
                }
            }
        }
    }

    private void UpdateDialog()
    {
        if (_dialogStartedTime > 0f && _dialogMinDuration > 0f)
        {
            if (!string.IsNullOrEmpty(_cachedSucceedRespText))
            {
                if (Time.realtimeSinceStartup > _dialogStartedTime + _dialogMinDuration)
                {
                    string tempText = _cachedSucceedRespText;
                    _dialogStartedTime = 0f;
                    _dialogMinDuration = 0f;
                    _cachedSucceedRespText = string.Empty;

                    PopupDialogCtrl._instance.Hide();
                    if (_succeedRespHandler != null)
                    {
                        _succeedRespHandler(tempText);
                    }
                }
            }
        }
    }

    public void login_fn()
    {
        ShowMinDialog(
            "登入中，請稍等",
            2f,
            (text) =>
            {
                ShowMinDialog(
                    "取得帳號資料\n請稍等",
                    2f,
                    (text2) =>
                    {
                        PopupDialogCtrl._instance.Hide();
                        OnLoginSucceed(text2);
                        ShowTutorial();
                    }
                );

                ProcessFetchAccountData((succeed) => {
                    if (succeed) {
                        _cachedSucceedRespText = "succeed";
                    }
                    else {
                        PopupDialogCtrl._instance.Setup("取得帳號資料失敗\n請重新登入");
                        PopupDialogCtrl._instance.Show(2f);
                    }
                });
            }
        );

        AppWebService._instance.RequestPlayerLogin(
            _inputAccount.text,
            _inputPassword.text,
            (succeedText) =>
            {
            #if DEBUG_LOG
                Debug.LogFormat("login_fn() success.");
            #endif
                _cachedSucceedRespText = succeedText;
            },
            (respText) =>
            {
            #if DEBUG_LOG
                Debug.LogFormat("login_fn() failed.");
            #endif
                Debug.LogError("Login Failed! text=" + respText);
                string errorMsg = string.Empty;
                AppWebService.ResponseBase respBase = JsonUtility.FromJson<AppWebService.ResponseBase>(respText);
                switch (respBase.errorCode)
                {
                    case 1:
                        errorMsg = "帳號/密碼錯誤\n請檢查後再試一次";
                        break;

                    default:
                        errorMsg = "未知錯誤\n請稍候再試一次";
                        break;
                }

                PopupDialogCtrl._instance.Setup("帳號/密碼錯誤\n請檢查後再試一次");
                PopupDialogCtrl._instance.Show(2.5f);
            }
        );
    }

    public void register_fn()
    {
        if (!VerifyRegData())
        {
            _registerUiCtrl.ShowHints(true);
            PopupDialogCtrl._instance.Setup("註冊資料不完整\n請參考提示做修正");
            PopupDialogCtrl._instance.Show(2f);
            return;
        }

        ShowMinDialog(
            "註冊中，請稍等",
            2f,
            (text) =>
            {
                //regi.SetActive(false);
                //regi_su.SetActive(true);
                boy_pic.SetActive(true);
                name_1.text = name.text;
                sex_1.text = _inputAccount.text;
                //sex_1.text = sex.text;
                /*if (sex_1.text == "男")
                {
                    boy_pic.SetActive(true);
                }
                if (sex_1.text == "女")
                {
                    girl_pic.SetActive(true);
                }*/

                _inputAccount.text = _inputRegUsername.text;
                _inputPassword.text = _inputRegPassword.text;

                OnLoginClicked();
            }
        );

        AppWebService._instance.RequestPlayerNew(
            _inputRegUsername.text,
            _inputRegPassword.text,
            _inputRegName.text,
            _textRegSex.text,
            _textBirthday.text,
            _textRegLocation.text,
            (succeedText) =>
            {
                _cachedSucceedRespText = succeedText;
            },
            (failedText) =>
            {
                string errorMsg = string.Empty;
                AppWebService.ResponseBase respBase = JsonUtility.FromJson<AppWebService.ResponseBase>(failedText);
                switch (respBase.errorCode)
                {
                    case 2:
                        errorMsg = "Email已存在\n請換Email再試一次";
                        break;

                    default:
                        errorMsg = "Email/密碼錯誤\n請檢查後再試一次";
                        break;
                }

                PopupDialogCtrl._instance.Setup(errorMsg);
                PopupDialogCtrl._instance.Show(2f);
            }
        );
    }
    public void info_fn()
    {
        _informationPanelCtrl.Show(!_informationPanelCtrl.IsContentsShowing());
    }
    public void reset_pos()
    {
        player_pos.transform.localPosition = new Vector3(-700, -250, 0);
    }
    
    public void point_fn(float aa, float bb, GameObject xx)
    {
        float la = aa;
        float lo = bb;

        for (int x = 0; x < 34; x++)
        {
            for (int y = 0; y < 38; y++)
            {
                point_cn = Instantiate(point, xx.transform);
                point_cn.transform.localPosition = new Vector3(posx, posy);
                posy = posy + 15;

                point_cn.GetComponent<point_SC>().latitude = la;
                point_cn.GetComponent<point_SC>().longitude = lo;
                la = la + 0.005f;
            }

            posx = posx + 15;
            posy = -250;
            la = aa;
            lo = lo + 0.0025f;
        }
        posx = -250;
        posy = -250;
    }

    public void OnBirthdayClicked()
    {
        if (_inputBirthdayPanel != null)
        {
            _inputBirthdayPanel.SetActive(true);
        }
    }

    public void OnBirthdayConfirmClicked()
    {
        if (_textBirthday != null)
        {
            _textBirthday.text = string.Format("{0}-{1}-{2}", _datepickerCtrl.GetYear(), _datepickerCtrl.GetMonth(), _datepickerCtrl.GetDay());
        }

        if (_inputBirthdayPanel != null)
        {
            _inputBirthdayPanel.SetActive(false);
        }
    }

    public void OnNavClicked()
    {
        PopupDialogCtrl._instance.Show("將開啟google地圖\n請在google地圖輸入尋寶點",
            () => {
                // http://maps.google.com/?q=口湖遊客中心
                Application.OpenURL("https://www.google.com/maps");
                PopupDialogCtrl._instance.Hide();
            }, 
            () => PopupDialogCtrl._instance.Hide()
        );

        /*
        if (_dialogConfirmLeaveCtrl != null) {
            _dialogConfirmLeaveCtrl.Setup(
                () => {
                    // http://maps.google.com/?q=口湖遊客中心
                    Application.OpenURL("https://www.google.com/maps");
                    _dialogConfirmLeaveCtrl.Show(false);
                }, 
                () => _dialogConfirmLeaveCtrl.Show(false)
            );
            _dialogConfirmLeaveCtrl.Show(true);
        }
        */
    }

    public void OnWebsiteClicked()
    {
        if (_dialogConfirmLeaveCtrl != null) {
            _dialogConfirmLeaveCtrl.Setup(
                () => {
                    Application.OpenURL("http://www.yunlinshop.com.tw/");
                    _dialogConfirmLeaveCtrl.Show(false);
                }, 
                () => _dialogConfirmLeaveCtrl.Show(false)
            );
            _dialogConfirmLeaveCtrl.Show(true);
        }
    }

    public void OnLoginClicked()
    {
        login_fn();
    }

    public void OnLogoutClicked()
    {
        PopupDialogCtrl._instance.Show("確定要登出?", 
            () => {
                AppWebService._instance.SaveToken(string.Empty);
                PopupDialogCtrl._instance.Hide();
                UnityEngine.SceneManagement.SceneManager.LoadScene("Yunlin");
            },
            null
        );
    }

    private void OnLoginSucceed(string text)
    {
    #if DEBUG_LOG
        Debug.Log("Login succeed! text=" + text);
    #endif

        _inputAccount.text = string.Empty;
        _inputPassword.text = string.Empty;
        login.SetActive(false);
        map.SetActive(true);
    }

    private bool VerifyRegData()
    {
        if (_inputRegUsername.text.Length >= 4
            && _inputRegPassword.text.Length >= 6
            && _inputRegName.text.Length >= 4
            && _textBirthday.text.Length > 0
            )
        {
            return true;
        }
        return false;
    }

    private void ShowMinDialog(string text, float minDuration, System.Action<string> callback)
    {
        PopupDialogCtrl._instance.Setup(text);
        PopupDialogCtrl._instance.Show();
        _dialogStartedTime = Time.realtimeSinceStartup;
        _dialogMinDuration = minDuration;
        _cachedSucceedRespText = string.Empty;
        _succeedRespHandler = callback;
    }

    private void ShowTutorial() {
        if (_tutorialCtrl != null) {
            _tutorialCtrl.Show(true);
        }
    }

    private void ProcessAutoLogin() {
        ShowMinDialog(
            "歡迎回來\n取得帳號資料中",
            2f,
            (text) =>
            {
                PopupDialogCtrl._instance.Hide();
                _gameObjHomepage.SetActive(false);
                OnLoginSucceed(string.Empty);
            }
        );

        ProcessFetchAccountData((succeed) => {
            if (succeed) {
                _cachedSucceedRespText = "succeed";
            }
            else {
                PopupDialogCtrl._instance.Setup("取得帳號資料失敗\n請重新登入");
                PopupDialogCtrl._instance.Show(2f);
            }
        });
    }

    private void ProcessFetchAccountData(System.Action<bool> completedCallback) {
        // Get Player Info
        AppWebService._instance.RequestGetPlayerInfo(
            (succeedText) =>
            {
                Debug.LogFormat("    RequestGetPlayerInfo succeed. text={0}", succeedText);
                AppWebService.ResponsePlayerInfo resp = JsonUtility.FromJson<AppWebService.ResponsePlayerInfo>(succeedText);
                PlayerDataCtrl._instance.SetName(resp.name);
                PlayerDataCtrl._instance.SetSex(resp.sex);
                PlayerDataCtrl._instance.SetScoreNames(resp.scores.names);
                PlayerDataCtrl._instance.SetOpenedQuestionNames(resp.openedQuestions.names);

                name_1.text = resp.name;
                sex_1.text = _inputAccount.text;
                //sex_1.text = resp.sex;
                boy_pic.SetActive(true);
                /*if (sex_1.text == "男")
                {
                    boy_pic.SetActive(true);
                }
                if (sex_1.text == "女")
                {
                    girl_pic.SetActive(true);
                }*/

                AppWebService._instance.RequestPlayerScoreAll(
                    (succeedText2) => {
                        if (completedCallback != null) {
                            completedCallback(true);
                        }
                    },
                    (respText2) => {
                        Debug.LogError("Get score all failed! text=" + respText2);       
                        if (completedCallback != null) {
                            completedCallback(false);
                        }
                    }
                );
            },
            (respText) =>
            {
                Debug.LogError("Get player info failed! text=" + respText);
                if (completedCallback != null) {
                    completedCallback(false);
                }
            }
        );
    }

    private void RefreshScoreStatus() {
        List<string> scoreNames = PlayerDataCtrl._instance.GetScoreNames();
        
        const int TOTAL_NUM_SCORE = 9;
        int numGotScore = scoreNames != null ? scoreNames.Count : 0;
        int numRemainScore = TOTAL_NUM_SCORE - numGotScore;

        if (_textGotScore != null) {
            _textGotScore.text = numGotScore.ToString();
        }

        if (_textRemainScore != null) {
            _textRemainScore.text = numRemainScore.ToString();
        }
    }
}
