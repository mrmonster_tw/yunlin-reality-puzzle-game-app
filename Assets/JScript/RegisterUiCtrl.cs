﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterUiCtrl : MonoBehaviour
{
    [SerializeField] private GameObject[] _gameObjHints;

    private void Start()
    {
        ShowHints(false);
    }

    public void ShowHints(bool show)
    {
        foreach (var gameObj in _gameObjHints)
        {
            gameObj.SetActive(show);
        }
    }
}
