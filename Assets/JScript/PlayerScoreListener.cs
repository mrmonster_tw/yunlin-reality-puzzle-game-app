﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ind.leoyintw.utils;

public class PlayerScoreListener : MonoBehaviour
{
    [SerializeField] private string _matchedScoreName;
    [SerializeField] private bool _show;

    private NoticeObserverComponent _noticeObserverComp;

    void Start()
    {
        _noticeObserverComp = gameObject.AddComponent<NoticeObserverComponent>();
        _noticeObserverComp._noticeObserver.Add((int)GameNotices.ScoreNameAdded, HandleScoreNameAdded);
        HandleScoreNameAdded(string.Empty);
    }

    void HandleScoreNameAdded(object dataObj) {
        string scoreName = (string)dataObj;
        gameObject.SetActive(PlayerDataCtrl._instance.HasScoreName(_matchedScoreName) ? _show : !_show);
    }
}
