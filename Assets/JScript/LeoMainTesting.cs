﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeoMainTesting : MonoBehaviour
{
    [SerializeField] private AlbumsCtrl _albumsCtrl;

    // Update is called once per frame
    void Update()
    {
        #if DEV
            if (Input.GetKeyDown(KeyCode.A)) {
                if (_albumsCtrl != null) {
                    _albumsCtrl.Setup(0);
                }
            }

            if (Input.GetKeyDown(KeyCode.S)) {
                if (_albumsCtrl != null) {
                    _albumsCtrl.Setup(1);
                }
            }

            if (Input.GetKeyDown(KeyCode.D)) {
                if (_albumsCtrl != null) {
                    _albumsCtrl.Setup(2);
                }
            }
        #endif
    }
}
