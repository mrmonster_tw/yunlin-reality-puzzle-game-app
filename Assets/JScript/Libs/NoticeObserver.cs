﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ind.leoyintw.utils {

    public class NoticeObserver {

        private Dictionary<int, NoticeService.OnReceived> _listeningMsgs;

        public NoticeObserver() {
            _listeningMsgs = new Dictionary<int, NoticeService.OnReceived>();
        }

        ~NoticeObserver() {
            RemoveAll();
        }

        public void Add(int msgID, NoticeService.OnReceived func) {
            if (func != null) {
                if (!_listeningMsgs.ContainsKey(msgID)) {
                    _listeningMsgs.Add(msgID, func);
                    NoticeService.AddObserver(msgID, func);
                }
                else {
                    Debug.LogWarningFormat("msgID:{0} is already listening.", msgID);
                }
            }
        }

        public void Remove(int msgID, NoticeService.OnReceived func) {
            if (func != null) {
                if (_listeningMsgs.ContainsKey(msgID)) {
                    _listeningMsgs.Remove(msgID);
                    NoticeService.RemoveObserver(msgID, func);
                }
            }
        }

        public void RemoveAll() {
            foreach (KeyValuePair<int, NoticeService.OnReceived> pair in _listeningMsgs) {
                NoticeService.RemoveObserver(pair.Key, pair.Value);
            }
            _listeningMsgs.Clear();
        }
    }
}
