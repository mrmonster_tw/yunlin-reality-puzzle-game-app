﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ind.leoyintw.utils {

    public class NoticeObserverComponent : MonoBehaviour {

        public NoticeObserver _noticeObserver = new NoticeObserver();

        private void OnDestroy() {
            if (_noticeObserver != null) {
                _noticeObserver.RemoveAll();
                _noticeObserver = null;
            }
        }
    }
}
