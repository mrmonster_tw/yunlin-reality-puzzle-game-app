﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ind.leoyintw.utils {

    public class NoticeService {

        public delegate void OnReceived(object dataObj);

        #region Static Members
        private static NoticeService _instance;
        private static NoticeService instance {
            get {
                if (_instance == null) {
                    _instance = new NoticeService();
                }
                return _instance;
            }
        }

        private static Dictionary<int, List<OnReceived>> observers = new Dictionary<int, List<OnReceived>>();

        public static void AddObserver(int noticeID, OnReceived func) {
            if (func != null) {
                if (!observers.ContainsKey(noticeID)) {
                    observers.Add(noticeID, new List<OnReceived>());
                }

                observers[noticeID].Add(func);
            }
        }

        public static void RemoveObserver(int noticeID, OnReceived func) {
            if (func != null) {
                if (observers.ContainsKey(noticeID)) {
                    observers[noticeID].Remove(func);
                }
            }
        }

        public static void SendNotice(int noticeID, object dataObj = null) {
            if (observers.ContainsKey(noticeID)) {
                List<OnReceived> funcs = new List<OnReceived>(observers[noticeID].ToArray());
                for (int i = 0; i < funcs.Count; ++i) {
                    funcs[i](dataObj);
                }
            }
        }
        #endregion
    }
}