﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ind.leoyintw.utils {
	
	public class SingletonService {

		private static Dictionary<Type, object> services = new Dictionary<Type, object>();

		public static bool Add<T>(object service) {
			if (service == null) {
				Debug.Log("service==null !");
				return false;
			}

			if (services.ContainsKey(typeof(T))) {
				Debug.Log("type exists !");
				return false;
			}

			services.Add(typeof(T), service);

			Debug.Log("service added.");
			return true;
		}

		public static T Get<T>() {
			if (services.ContainsKey(typeof(T))) {
				return (T)services[typeof(T)];
			}
			return default(T);
		}

		public static bool Has<T>() {
			return services.ContainsKey(typeof(T));
		}
	}
}

