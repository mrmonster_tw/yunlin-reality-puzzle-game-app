﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HttpService : MonoBehaviour
{
    static private HttpService _instance;

    private void Awake()
    {
		if (_instance != null)
		{
			Destroy(this);
			return;
		}
		_instance = this;
	}

	private void OnDestroy()
	{
        Debug.Log("HttpService.OnDestroy");
		if (_instance == this)
		{
			_instance = null;
		}
	}

    static public void SendPost(string url, WWWForm wwwForm, string token, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        if (_instance == null)
        {
            Debug.LogError("HttpService not exist!");
            return;
        }

        _instance.DoPost(url, wwwForm, token, succeedCallback, failedCallback);
    }

    static public void SendGet(string url, Dictionary<string, string> parameters, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        if (_instance == null)
        {
            Debug.LogError("HttpService not exist!");
            return;
        }

        _instance.DoGet(url, parameters, succeedCallback, failedCallback);
    }

    private void DoPost(string url, WWWForm wwwForm, string token, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        StartCoroutine(POST(url, wwwForm, token, succeedCallback, failedCallback));
    }

    private void DoGet(string url, Dictionary<string, string> parameters, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        StartCoroutine(GET(url, parameters, succeedCallback, failedCallback));
    }

    private IEnumerator POST(string url, WWWForm wwwForm, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        yield return new WaitForEndOfFrame();

        UnityWebRequest www = UnityWebRequest.Post(url, wwwForm);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
            if (failedCallback != null)
            {
                failedCallback(www.error);
            }
        }
        else
        {
            Debug.Log("Post complete! text=" + www.downloadHandler.text);

            string s = www.GetResponseHeader("set-cookie");
            Debug.Log("set-cookie=" + s);

            if (succeedCallback != null)
            {
                succeedCallback(www.downloadHandler.text);
            }
        }
    }

    private IEnumerator POST(string url, WWWForm wwwForm, string token, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        yield return new WaitForEndOfFrame();

        UnityWebRequest www = UnityWebRequest.Post(url, wwwForm);
        if (!string.IsNullOrEmpty(token))
        {
            www.SetRequestHeader("AUTHORIZATION", "Basic " + token);
        }

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error + " text=" + www.downloadHandler.text);
            if (failedCallback != null)
            {
                failedCallback(www.downloadHandler.text);
            }
        }
        else
        {
            Debug.Log("Post complete! text=" + www.downloadHandler.text);
            if (succeedCallback != null)
            {
                succeedCallback(www.downloadHandler.text);
            }
        }
    }

    private IEnumerator GET(string url, Dictionary<string, string> parameters, System.Action<string> succeedCallback, System.Action<string> failedCallback)
    {
        yield return new WaitForEndOfFrame();

        if (parameters != null && parameters.Count > 0)
        {
            url += "?";

            int counter = 0;
            foreach (var pair in parameters)
            {
                counter++;
                if (counter > 1)
                {
                    url += "&";
                }
                url += string.Format("{0}={1}", pair.Key, pair.Value);
            }
        }
        Debug.Log("GET() Final url=" + url);

        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
            if (failedCallback != null)
            {
                failedCallback(www.error);
            }
        }
        else
        {
            Debug.Log("Get complete! text=" + www.downloadHandler.text);
            if (succeedCallback != null)
            {
                succeedCallback(www.downloadHandler.text);
            }
        }
    }

    #region Public Static Methods
    public static string DecodeUnicodeStr(string unicodeStr)
    {
        byte[] bytes = System.Text.Encoding.Unicode.GetBytes(unicodeStr);
        return System.Text.Encoding.Unicode.GetString(bytes);
    }
    #endregion
}
