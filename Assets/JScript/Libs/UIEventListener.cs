﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UIEventListener : MonoBehaviour, IPointerDownHandler, IPointerClickHandler,
	IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler,
	IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public System.Action<PointerEventData> _onBeginDrag;
    public System.Action<PointerEventData> _onDrag;
    public System.Action<PointerEventData> _onEndDrag;

    static public UIEventListener Get(GameObject gameObj)
	{
        if (gameObj != null)
		{
			UIEventListener listener = gameObj.GetComponent<UIEventListener>();
            if (listener == null)
			{
				listener = gameObj.AddComponent<UIEventListener>();
			}
			return listener;
		}
		return null;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		//Debug.Log("Drag Begin");
        if (_onBeginDrag != null)
        {
            _onBeginDrag(eventData);
        }
	}

	public void OnDrag(PointerEventData eventData)
	{
		//Debug.Log("Dragging");
        if (_onDrag != null)
        {
            _onDrag(eventData);
        }
    }

	public void OnEndDrag(PointerEventData eventData)
	{
		//Debug.Log("Drag Ended");
        if (_onEndDrag != null)
        {
            _onEndDrag(eventData);
        }
    }

	public void OnPointerClick(PointerEventData eventData)
	{
		//Debug.Log("Clicked: " + eventData.pointerCurrentRaycast.gameObject.name);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		//Debug.Log("Mouse Down: " + eventData.pointerCurrentRaycast.gameObject.name);
    }

	public void OnPointerEnter(PointerEventData eventData)
	{
		//Debug.Log("Mouse Enter");
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		//Debug.Log("Mouse Exit");
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		//Debug.Log("Mouse Up");
	}
}