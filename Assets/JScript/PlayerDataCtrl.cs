﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ind.leoyintw.utils;

public class PlayerDataCtrl : MonoBehaviour
{
	public static PlayerDataCtrl _instance;

    private string _name = string.Empty;
    private string _sex = string.Empty;
    private List<string> _scoreNames = new List<string>();
    private List<string> _openedQuestionNames = new List<string>();

    public delegate void ScoreNameAddedHandler(string scoreName);

    private void Awake()
	{
		if (_instance != null)
		{
			Destroy(this);
			return;
		}
		_instance = this;
	}

	private void OnDestroy()
	{
        Debug.Log("PlayerDataCtrl.OnDestroy");
		if (_instance == this)
		{
            Debug.Log(" 0");
			_instance = null;
		}
	}

    public int GetNumScoreNames()
    {
        return _scoreNames.Count;
    }

    public string GetScoreName(int index)
    {
        if (index >= 0 && index < _scoreNames.Count)
        {
            return _scoreNames[index];
        }
        return string.Empty;
    }

    public void AddScoreName(string name)
	{
		_scoreNames.Add(name);
        NoticeService.SendNotice((int)GameNotices.ScoreNameAdded, name);
	}

    public void SetScoreNames(string[] names)
	{
        Debug.LogFormat("SetScoreNames()");
        for (int i = 0; i < names.Length; ++i)
        {
            Debug.LogFormat("    name[{1}]={0}", names[i], i);
        }

        _scoreNames = new List<string>(names);
        NoticeService.SendNotice((int)GameNotices.ScoreChanged);
    }

    public List<string> GetScoreNames()
    {
        return _scoreNames;
    }

    public void Reset()
	{
		_scoreNames = new List<string>();
	}

    public bool HasScoreName(string scoreName)
    {
        foreach (var name in _scoreNames)
        {
            if (name.IndexOf(scoreName) >= 0)
            {
                return true;
            }
        }
        return false;
    }

    public void SetName(string name)
    {
        _name = name;
    }

    public string GetName()
    {
        return _name;
    }

    public void SetSex(string sex)
    {
        _sex = sex;
    }

    public string GetSex()
    {
        return _sex;
    }

    public void SetOpenedQuestionNames(string[] names)
    {
        Debug.LogFormat("SetOpenedQuestionNames()");
        for (int i = 0; i < names.Length; ++i)
        {
            Debug.LogFormat("    name[{1}]={0}", names[i], i);
        }

        _openedQuestionNames = new List<string>(names);
    }

    public void AddOpenedQuestionName(string name)
    {
        if (!HasOpenedQuestionName(name))
        {
            _openedQuestionNames.Add(name);
        }
    }

    public bool HasOpenedQuestionName(string name)
    {
        if (_openedQuestionNames != null)
        {
            return _openedQuestionNames.Contains(name);
        }
        return false;
    }
}
