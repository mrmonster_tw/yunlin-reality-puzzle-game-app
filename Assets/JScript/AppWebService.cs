﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AppWebService : MonoBehaviour
{
    [System.Serializable]
    public class ResponseBase
    {
        public string message;
        public int errorCode;
    }

    [System.Serializable]
    public class ResponseLogin
    {
        public string token;
    }

    [System.Serializable]
    public class ResponsePlayerScores
    {
        public string[] names;
    }

    [System.Serializable]
    public class ResponseOpenedQuestions
    {
        public string[] names;
    }

    [System.Serializable]
    public class ResponsePlayerInfo
    {
        public string name;
        public string sex;
        public ResponsePlayerScores scores;
        public ResponseOpenedQuestions openedQuestions;
    }

#if !DEV
    private const string CFG_URL_BASE = "https://www.mrmonsters.com/yunlin/client-cfg/";
#else
    private const string CFG_URL_BASE = "https://www.mrmonsters.com/yunlin-dev/client-cfg/";
#endif

#if !DEV
    private const string URL_BASE = "https://www.mrmonsters.com/yunlin/api/";
#else
    private const string URL_BASE = "https://www.mrmonsters.com/yunlin-dev/api/";
#endif
    private const string URL = URL_BASE + "yunlin_ar_puzzle_game.php";

	public static AppWebService _instance;

    private const string PREFS_TOKEN = "token";

    private string _token = string.Empty;

    private void Awake()
	{
		if (_instance != null)
		{
			Destroy(this);
			return;
		}
		_instance = this;
        LoadToken();
	}

    private void OnDestroy()
	{
        Debug.Log("AppWebService.OnDestroy");
        if (_instance == this)
		{
            Debug.Log(" 0");
			_instance = null;
		}
	}

	public void RequestPlayerNew(string username, string password, string name, string sex, string birthday, string location, System.Action<string> succeedCallback, System.Action<string> failedcalback)
	{
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField("action", "playerNew");
		wwwForm.AddField("username", username);
		wwwForm.AddField("password", password);
		wwwForm.AddField("name", name);
		wwwForm.AddField("sex", sex);
        wwwForm.AddField("birthday", birthday);
        wwwForm.AddField("location", location);

		HttpService.SendPost(URL, wwwForm, string.Empty, succeedCallback, failedcalback);
	}

	public void RequestPlayerLogin(string username, string password, System.Action<string> succeedCallback, System.Action<string> failedcalback)
	{
    #if DEBUG_LOG
        Debug.LogFormat("RequestPlayerLogin() username:{0}. password:{1}.", username, password);
    #endif

		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField("action", "playerLogin");
		wwwForm.AddField("username", username);
		wwwForm.AddField("password", password);

		HttpService.SendPost(URL, wwwForm, string.Empty,
            (text) =>
            {
            #if DEBUG_LOG
                Debug.LogFormat("RequestPlayerLogin() success.");
            #endif
                ResponseLogin resp = JsonUtility.FromJson<ResponseLogin>(text);
                _token = resp.token;
                SaveToken(_token);
                if (succeedCallback != null)
                {
                    succeedCallback(text);
                }
            },
            failedcalback
        );
	}

	public void RequestPlayerScoreNew(string scoreName, System.Action<string> succeedCallback, System.Action<string> failedcalback)
	{
        Debug.LogWarning("RequestScore. scoreName=" + scoreName);
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField("action", "playerScoreNew");
		wwwForm.AddField("score_name", scoreName);

		HttpService.SendPost(URL, wwwForm, _token, succeedCallback, failedcalback);
	}

	public void RequestPlayerScoreAll(System.Action<string> succeedCallback, System.Action<string> failedcalback)
	{
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField("action", "playerScoreAll");

		HttpService.SendPost(URL, wwwForm, _token, succeedCallback, failedcalback);
	}

    public void RequestGetPlayerInfo(System.Action<string> succeedCallback, System.Action<string> failedcalback)
    {
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("action", "getPlayerInfo");

        HttpService.SendPost(URL, wwwForm, _token, succeedCallback, failedcalback);
    }

    public void RequestOpenedQuestionNew(string questionName, System.Action<string> succeedCallback, System.Action<string> failedcalback)
    {
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("action", "playerOpenedQuestionNew");
        wwwForm.AddField("question_name", questionName);

        HttpService.SendPost(URL, wwwForm, _token, succeedCallback, failedcalback);
    }

    public IEnumerator RequestDownloadTxt(string url, System.Action<bool, string> callback) {
    #if DEBUG_LOG
        Debug.LogFormat("RequestDownloadTxt() <color=blue>url={0}.</color>", url);
    #endif

        yield return new WaitForEndOfFrame();   

        var request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

    #if DEBUG_LOG
        Debug.Log("Status Code: " + request.responseCode);
    #endif

        if (!(request.isHttpError || request.isNetworkError)) {
            if (request.downloadHandler != null) {
            #if DEBUG_LOG
                Debug.Log("download.text: " + request.downloadHandler.text);
            #endif

                if (callback != null) {
                    callback(true, request.downloadHandler.text);
                }
            }
            else {
                if (callback != null) {
                    callback(true, string.Empty);
                }
            }
        }
        else {
            if (callback != null) {
                callback(false, string.Format("Status Code: {0}", request.responseCode));
            }             
        }
    }    

    public void SaveToken(string token)
    {
        _token = token;
        PlayerPrefs.SetString(PREFS_TOKEN, _token);
        PlayerPrefs.Save();
    }

    private void LoadToken()
    {
        _token = PlayerPrefs.GetString(PREFS_TOKEN, string.Empty);
    }

    public bool HasToken()
    {
        return !string.IsNullOrEmpty(_token);
    }
}
