﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusListCtrl : MonoBehaviour
{
    [SerializeField] private GameObject _contents;
    [SerializeField] private BonusCtrl _bonusCtrl;

    // Start is called before the first frame update
    void Start()
    {
        Show(false);
    }

    public void Show(bool show) {
        if (_contents != null) {
            _contents.SetActive(show);
        }
    }

    public void OnBackClicked() {
        Show(false);
    }

    // Update is called once per frame
    public void OnBonusClicked(int index) {
        int neededNumScores = (index + 1) * 3;
        switch (index) {
            case 0:
                if (PlayerDataCtrl._instance.GetNumScoreNames() >= neededNumScores) {
                    _bonusCtrl.Show(BonusCtrl.BonusType.Small, true);
                }
                else {
                    string msg = "答對三題，可以獲得此獎勵";
                    PopupDialogCtrl._instance.Show(msg, null);
                }
                break;

            case 1:
                if (PlayerDataCtrl._instance.GetNumScoreNames() >= neededNumScores) {
                    _bonusCtrl.Show(BonusCtrl.BonusType.Big, true);
                }
                else {
                    string msg = "答對六題，可以獲得此獎勵";
                    PopupDialogCtrl._instance.Show(msg, null);
                }
                break;

            case 2: 
                if (PlayerDataCtrl._instance.GetNumScoreNames() >= neededNumScores) {
                    _bonusCtrl.Show(BonusCtrl.BonusType.Small, true);
                }
                else {
                    string msg = "答對九題，可以獲得此獎勵";
                    PopupDialogCtrl._instance.Show(msg, null);
                }
                break;
        }
    }
}
