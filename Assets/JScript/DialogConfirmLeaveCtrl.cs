﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogConfirmLeaveCtrl : MonoBehaviour
{
    [SerializeField] private GameObject _contents;

    private System.Action _okCallback;
    private System.Action _cancelCallback;

    private void Start() {
        Show(false);    // hide in default.
    }

    public void Setup(System.Action okCallback, System.Action cancelCallback) {
        _okCallback = okCallback;
        _cancelCallback = cancelCallback;
    }

    public void OnOkClicked() {
        if (_okCallback != null) {
            _okCallback();
        }
    }

    public void OnCancelClicked() {
        if (_cancelCallback != null) {
            _cancelCallback();
        }
    }

    public void Show(bool show) {
        if (_contents != null) {
            _contents.SetActive(show);
        }
    }
}
