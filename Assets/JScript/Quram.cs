﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quram : MonoBehaviour
{
    public GameObject main_sc;
    public GameObject q_1;
    public GameObject q_2;


    // Start is called before the first frame update
    void Start()
    {
        main_sc = GameObject.FindGameObjectWithTag("Main");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ram_fn()
    {
        if (main_sc.GetComponent<Main>().q_ramdon == 1)
        {
            q_1.SetActive(true);
        }
        if (main_sc.GetComponent<Main>().q_ramdon == 2)
        {
            q_2.SetActive(true);
        }
    }
}
